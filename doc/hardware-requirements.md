# So you want to give us hardware? Great! Here's what we need...

If you want to [donate hardware][], there are specific requirements
for machine we manage that you should follow. For other donations,
please see the [donation site][].

[donate hardware]: https://donate.torproject.org/donor-faq
[donation site]: https://donate.torproject.org/

This list is not final, and if you have questions, please [contact
us](doc/how-to-get-help).

## Must have

 * Out of band management with dedicated network port, preferably a
   something standard (like serial-over-ssh, with BIOS redirection),
   or failing that, serial console and networked power bars
 * No human intervention to power on or reboot
 * Warranty or post-warranty hardware support, preferably provided by
   the sponsor
 * Under the 'ownership' of Tor, although long-term loans can also
   work
 * Rescue system (PXE bootable OS or remotely loadeable ISO image)

## Nice to have

 * Production quality rather than pre-production hardware
 * Support for multiple drives (so we can do RAID) although this can
   be waived for disposable servers like build boxes
 * Hosting for the machine: we do not run our own datacenters or rack,
   so it would be preferable if you can also find a hosting location
   for the machine, ideally dual-stack (IPv4 and IPv6) and gigabit or
   greater

## To avoid

 * proprietary Java/ActiveX remote consoles
 * hardware RAID, unless supported with open drivers in the mainline
   Linux kernel and userland utilities
