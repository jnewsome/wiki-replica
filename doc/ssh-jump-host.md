---
title: learning how to do an ssh jump host on tpo
---

You need to use an ssh jump host to access internal machines at tpo.
If you have a recent enough ssh (>= 2016 or so), then you can use the `ProxyJump` directive.  Else, use `ProxyCommand`.
`ProxyCommand` automatically executes the ssh command on the host to jump to the next host and forward all traffic through.

With recent ssh versions:

    Host *.torproject.org !people.torproject.org
      ProxyJump people.torproject.org

Or with old ssh versions (before OpenSSH 7.3, or Debian 10 "buster"):

    Host *.torproject.org !people.torproject.org
      ProxyCommand ssh -l %r -W %h:%p people.torproject.org

If your local username is different from your TPO username, also set
it in your `.ssh/config`:

    Host *.torproject.org
      User USERNAME

It is also worth keeping the `known_hosts` file in sync to avoid
server authentication warnings. The server's public keys are also
available in DNS. So add this to your `.ssh/config`:

    Host *.torproject.org
      UserKnownHostsFile ~/.ssh/known_hosts.torproject.org
      VerifyHostKeyDNS ask

And keep the `~/.ssh/known_hosts.torproject.org` file up to date by
regularly pulling it from a TPO host, so that new hosts are
automatically added, for example:

    rsync -ctvLP perdulce.torproject.org:/etc/ssh/ssh_known_hosts ~/.ssh/known_hosts.torproject.org
